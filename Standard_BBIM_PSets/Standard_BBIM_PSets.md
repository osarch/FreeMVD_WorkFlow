#  BBIM_Array (Arrays)

## Single Dimensional Array

### The Parent
```
#97=IFCRELDEFINESBYPROPERTIES('3YTz4$0TPDCRRyNaSM6Woe',$,$,$,(#66),#96);
    #66=IFCBUILDINGELEMENTPROXY('0ceJ0YMb9AZgBY7SnTU7uk',$,'Cube',$,$,#95,#84,$,.COMPLEX.);
    #96=IFCPROPERTYSET('0dsZ6BPAr51vI1TL5nyHbI',$,' BBIM_Array',$,(#98,#99));
        #98=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('0ceJ0YMb9AZgBY7SnTU7uk'),$);
        #99=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('[
            {
                "children": ["2Xks_kocT50x4$uFU7_nC8", "1jXJoM7TTFcxzHQy8fgflb"],
                "count": 3,
                "x": 3.0,
                "y": 0.0,
                "z": 0.0,
                "use_local_space": true,
                "sync_children": true,
                "method": "OFFSET"
            }]'),$);
```



### The (2) Children

Example of how the children elements are related back to the parent.
```
#152=IFCRELDEFINESBYPROPERTIES('1b4aP4$PD1h8ZOri9N$Bs3',$,$,$,(#146),#153);
    #146=IFCBUILDINGELEMENTPROXY('2Xks_kocT50x4$uFU7_nC8',$,'Cube',$,$,#196,#156,$,.COMPLEX.);
    #153=IFCPROPERTYSET('1Colq2sofAAue65rpHZfO2',$,' BBIM_Array',$,(#154,#155));
        #154=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('0ceJ0YMb9AZgBY7SnTU7uk'),$);
        #155=IFCPROPERTYSINGLEVALUE('Data',$,$,$);

#175=IFCRELDEFINESBYPROPERTIES('2fu3bLYcL5fQbLFFfY0QgH',$,$,$,(#169),#176);
    #169=IFCBUILDINGELEMENTPROXY('1jXJoM7TTFcxzHQy8fgflb',$,'Cube',$,$,#201,#179,$,.COMPLEX.);
    #176=IFCPROPERTYSET('1RF6yywUj6ev9I2wza0UC$',$,' BBIM_Array',$,(#177,#178));
        #177=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('0ceJ0YMb9AZgBY7SnTU7uk'),$);
        #178=IFCPROPERTYSINGLEVALUE('Data',$,$,$);

```


### Properties - BBIM


####  BBIM_Array.Parent

|  Key |  Type |  Description |
|---|---|---|
|   |  string |  Parent's GlobalId |


####  BBIM_Array.Data

|  Key |  Type |  Description |
|---|---|---|
| children  |  list |  List of children's GlobalId |
|  count | integer  |   |
|  x | float |   |
|  y |  float |   |
|  z | float  |   |
|  use_local_space |  boolean | If TRUE, uses local space for array items offset instead of world space  |
|  sync_children | boolean  |  If TRUE, change in parent propagates to children.  If FALSE, it does not |
|  method |  enumeration  |  (OFFSET, DISTRIBUTE) <br>OFFSET: dimension between the parent and 1st child <br>DISTRIBUTE: dimension between the parent and last child |

File: [Array_Single_Dimensional.ifc]( BBIM_Array/Array_Single_Dimensional.ifc)
<br>![]( BBIM_Array/Array_Single_Dimensional.png)

## Multiple Dimensional Array

Example of a multi-dimensional array in the X, Y, Z directions. 

```
#97=IFCRELDEFINESBYPROPERTIES('3YTz4$0TPDCRRyNaSM6Woe',$,$,$,(#66),#96);
    #66=IFCBUILDINGELEMENTPROXY('0ceJ0YMb9AZgBY7SnTU7uk',$,'Cube',$,$,#95,#84,$,.COMPLEX.);
    #96=IFCPROPERTYSET('0dsZ6BPAr51vI1TL5nyHbI',$,' BBIM_Array',$,(#98,#99));
        #98=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('0ceJ0YMb9AZgBY7SnTU7uk'),$);
        #99=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('[{
            "children": ["3pdfrPXyb7jOFkyp$NOX$_", "0NZW6XVpH1U9TQ512yGMWv"],
            "count": 3,
            "x": 3.0,
            "y": 0.0,
            "z": 0.0,
            "use_local_space": true,
            "sync_children": true,
            "method": "OFFSET"
            }, {
            "children": ["2dx_KM4xHEvBR4EngKfG9I", "02kjA8gfz0SwCd0ZpoXQbf", "2faE2rWOXFQhPUwwvL4NSx"],
            "count": 2,
            "x": 0.0,
            "y": 3.0,
            "z": 0.0,
            "use_local_space": true,
            "sync_children": true,
            "method": "OFFSET"
            }, {
            "children": ["3xHyZLJbjFiQMaovZr5wG5", "0UEAfgNzvDQeKyWwCenNM9", "1CdFPCb$bA1PMl$piceJ$u", "0geFXRscn6we3TcqEtrDIQ", "28vUW8PDHDJh_mkZWx4DLD", "1aSkrcFCj7BuP3T9jIdkee"],
            "count": 2,
            "x": 0.0,
            "y": 0.0,
            "z": 3.0,
            "use_local_space": true,
            "sync_children": true,
            "method": "OFFSET"
            }]'),$);

```
File: [Array_Multi_Dimensional.ifc]( BBIM_Array/Array_Multi_Dimensional.ifc)
<br>![]( BBIM_Array/Array_Multi_Dimensional.png)




#  BBIM_Aggregate_Data (Aggregates Types, or Duplicate Aggregates)

### The Parent

```
#138=IFCRELDEFINESBYPROPERTIES('1YSUtLsBT1ggamuw2tmRnN',$,$,$,(#115),#137);
  #115=IFCELEMENTASSEMBLY('0oynFQ0yb17egHEYyYh1Lx',$,'Assembly',$,$,#125,$,$,$,$);
  #137=IFCPROPERTYSET('3R4Ebi$S94ag0FWbLTaKRn',$,' BBIM_Aggregate_Data',$,(#139,#140));
    #139=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('0oynFQ0yb17egHEYyYh1Lx'),$);
    #140=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('[{
        "children": ["1JZjm8A692EwIKy0igrKom", "3_nt6W8BLF8PuRrcJS_vfx"], 
        "instance_of": ["0oynFQ0yb17egHEYyYh1Lx"]
        }]'),$);

```


### The Children

```
#155=IFCRELDEFINESBYPROPERTIES('2atMwkrmDEbeY21cmaX$Pl',$,$,$,(#149),#156);
  #149=IFCELEMENTASSEMBLY('2F$GLpsjjAexY5TJxYUnr6',$,'Assembly',$,$,#230,$,$,$,$);
  #156=IFCPROPERTYSET('2ErUeSq0zAkv4aKV$j4fJy',$,' BBIM_Aggregate_Data',$,(#157,#158));
    #157=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('2F$GLpsjjAexY5TJxYUnr6'),$);
    #158=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('[{
        "children": ["3_FpqRIE14refMbOMLhG1$", "1_CYcGs$56NujayWqkmVz6"], 
        "instance_of": ["0oynFQ0yb17egHEYyYh1Lx"]
        }]'),$);

#257=IFCRELDEFINESBYPROPERTIES('0R7P6lkwD4lvL0PVS1Lztx',$,$,$,(#251),#258);
  #251=IFCELEMENTASSEMBLY('1TUwWgp3T9kho34Tm7UMOm',$,'Assembly',$,$,#332,$,$,$,$);
  #258=IFCPROPERTYSET('1LONBwPxDC0enmX6dcy9Z_',$,' BBIM_Aggregate_Data',$,(#259,#260));
    #259=IFCPROPERTYSINGLEVALUE('Parent',$,IFCLABEL('1TUwWgp3T9kho34Tm7UMOm'),$);
    #260=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('[{
        "children": ["22ik3Byx15lfBixUiP65nk", "0Lkz2fIkjF3x8EMzN33_0t"], 
        "instance_of": ["0oynFQ0yb17egHEYyYh1Lx"]
        }]'),$);

```

### Properties - BBIM

 BBIM_Aggregate_Data.Parent

|  Key |  Type |  Description |
|---|---|---|
|   |  string |  Parent's GlobalId |


 BBIM_Aggregate_Data.Data

|  Key |  Type |  Description |
|---|---|---|
| children  |  list |  A list of the elements (GlobalId) inside the aggregate |
| instance_of  |  list |  The GlobalId of the parent aggregate. If the aggregate 'is' the parent, the GlobalId will be the parent's GlobalId.  |


File: [ BBIM_Aggregate_Data.ifc]( BBIM_Aggregate_Data/ BBIM_Aggregate_Data.ifc)
<br>![]( BBIM_Aggregate_Data/ BBIM_Aggregate_Data.gif)


#  BBIM_Stair


### GENERIC

```

#108=IFCRELDEFINESBYTYPE('0ntSKBk6b8OOu_EKPVXGmC',$,$,$,(#89),#66);
  #89=IFCSTAIRFLIGHT('1gBKllPun5zQjABbX$9eJE',$,'StairFlight',$,$,#94,#116,$,7,6,0.142857142857143,0.25,.STRAIGHT.);
  #66=IFCSTAIRFLIGHTTYPE('3iQXKbg8n8CwP9Yfk3fRl9',$,'TYPEX',$,$,(#124,#126),(#88,#301),$,$,.STRAIGHT.);
    #124=IFCPROPERTYSET('0go5$W3vbF$gQVVEUPlHZx',$,' BBIM_Stair',$,(#125));
      #125=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('{
        "stair_type": "GENERIC", 
        "width": 1.2000000476837158, 
        "height": 1.0, 
        "number_of_treads": 6, 
        "tread_run": 0.30000001192092896
        }'),$);
    #126=IFCPROPERTYSET('2AiqI0d91Fphaqvf3FP66X',$,'Pset_StairFlightCommon',$,(#127,#128,#129,#130));
      #127=IFCPROPERTYSINGLEVALUE('NumberOfRiser',$,IFCCOUNTMEASURE(7.),$);
      #128=IFCPROPERTYSINGLEVALUE('NumberOfTreads',$,IFCCOUNTMEASURE(6.),$);
      #129=IFCPROPERTYSINGLEVALUE('RiserHeight',$,IFCPOSITIVELENGTHMEASURE(0.142857142857143),$);
      #130=IFCPROPERTYSINGLEVALUE('TreadLength',$,IFCPOSITIVELENGTHMEASURE(0.25),$);

```
File: [ BBIM_Stair_Generic.ifc]( BBIM_Stair\ BBIM_Stair_Generic.ifc)
<br>![]( BBIM_Stair/ BBIM_Stair_Generic.png)

### CONCRETE

```
#108=IFCRELDEFINESBYTYPE('0ntSKBk6b8OOu_EKPVXGmC',$,$,$,(#89),#66);
  #89=IFCSTAIRFLIGHT('1gBKllPun5zQjABbX$9eJE',$,'StairFlight',$,$,#94,#116,$,7,6,0.142857142857143,0.25,.STRAIGHT.);
  #66=IFCSTAIRFLIGHTTYPE('3iQXKbg8n8CwP9Yfk3fRl9',$,'TYPEX',$,$,(#124,#126),(#88,#203),$,$,.STRAIGHT.);
    #124=IFCPROPERTYSET('0go5$W3vbF$gQVVEUPlHZx',$,' BBIM_Stair',$,(#125));
      #125=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('{
        "stair_type": "CONCRETE", 
        "width": 1.2000000476837158, 
        "height": 1.0, 
        "number_of_treads": 6, 
        "tread_run": 0.30000001192092896, 
        "base_slab_depth": 0.25, 
        "top_slab_depth": 0.25, 
        "has_top_nib": true, 
        "tread_depth": 0.25
        }'),$);
    #126=IFCPROPERTYSET('2AiqI0d91Fphaqvf3FP66X',$,'Pset_StairFlightCommon',$,(#127,#128,#129,#130));
      #127=IFCPROPERTYSINGLEVALUE('NumberOfRiser',$,IFCCOUNTMEASURE(7.),$);
      #128=IFCPROPERTYSINGLEVALUE('NumberOfTreads',$,IFCCOUNTMEASURE(6.),$);
      #129=IFCPROPERTYSINGLEVALUE('RiserHeight',$,IFCPOSITIVELENGTHMEASURE(0.142857142857143),$);
      #130=IFCPROPERTYSINGLEVALUE('TreadLength',$,IFCPOSITIVELENGTHMEASURE(0.25),$);


```
File: [ BBIM_Stair_Concrete.ifc]( BBIM_Stair/ BBIM_Stair_Concrete.ifc)
<br>![]( BBIM_Stair/ BBIM_Stair_Concrete.png)



### WOOD/STEEL

```
#108=IFCRELDEFINESBYTYPE('0ntSKBk6b8OOu_EKPVXGmC',$,$,$,(#89),#66);
  #89=IFCSTAIRFLIGHT('1gBKllPun5zQjABbX$9eJE',$,'StairFlight',$,$,#94,#116,$,7,6,0.142857142857143,0.25,.STRAIGHT.);
  #66=IFCSTAIRFLIGHTTYPE('3iQXKbg8n8CwP9Yfk3fRl9',$,'TYPEX',$,$,(#124,#126),(#88,#264),$,$,.STRAIGHT.);
    #124=IFCPROPERTYSET('0go5$W3vbF$gQVVEUPlHZx',$,' BBIM_Stair',$,(#125));
      #125=IFCPROPERTYSINGLEVALUE('Data',$,IFCLABEL('{
        "stair_type": "WOOD/STEEL", 
        "width": 1.2000000476837158, 
        "height": 1.0, 
        "number_of_treads": 6, 
        "tread_run": 0.30000001192092896, 
        "tread_depth": 0.25}'),$);
    #126=IFCPROPERTYSET('2AiqI0d91Fphaqvf3FP66X',$,'Pset_StairFlightCommon',$,(#127,#128,#129,#130));
      #127=IFCPROPERTYSINGLEVALUE('NumberOfRiser',$,IFCCOUNTMEASURE(7.),$);
      #128=IFCPROPERTYSINGLEVALUE('NumberOfTreads',$,IFCCOUNTMEASURE(6.),$);
      #129=IFCPROPERTYSINGLEVALUE('RiserHeight',$,IFCPOSITIVELENGTHMEASURE(0.142857142857143),$);
      #130=IFCPROPERTYSINGLEVALUE('TreadLength',$,IFCPOSITIVELENGTHMEASURE(0.25),$);

```


File: [ BBIM_Stair_Wood_Steel.ifc]( BBIM_Stair/ BBIM_Stair_Wood_Steel.ifc)
<br>![]( BBIM_Stair/ BBIM_Stair_Wood_Steel.png)

### Properties - BBIM


|  Key |  Type |  Applies to | Description |
|---|---|---|---|
| stair_type  |  enumeration |  | (GENERIC, CONCRETE, WOOD/STEEL) |
| width  |  float | ALL |  |
| height  |  float | ALL |  |
| number_of_treads  |  integer | ALL |  |
| tread_run  |  float | ALL |  |
| base_slab_depth  |  float | CONCRETE |  |
| has_top_nib  |  float | CONCRETE |  |
| tread_depth  |  float | CONCRETE, WOOD/STEEL |  |

### Properties - BS

- [Pset_StairFlightCommon](https://ifc43-docs.standards.buildingsmart.org/IFC/RELEASE/IFC4x3/HTML/lexical/Pset_StairFlightCommon.htm)